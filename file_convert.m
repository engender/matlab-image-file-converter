% Script for converting labview created binary files to image files

close all
clc

input_binary_file_name = 'nofibrelensnolaser';
output_image_name = input_binary_file_name;

%get input file info
file_info = dir(input_binary_file_name);

% open binary file
fid = fopen(file_info.name);

% get number of images stored in binary file
frames = get_number_of_frames(fid, file_info.bytes);

% initialize video writer object and open
writerObj = VideoWriter(strcat(input_binary_file_name, '.avi'));
open(writerObj);

for i = 1:frames;
    
    %read 20 bytes which contain image meta information as 5 32bit numbers
    meta_data = get_meta_data(fid);
    image_data = get_image_data(fid, meta_data);
    image_name = strcat(output_image_name, '_', num2str(i), '.png');

%     if (mod(i,2)); %uncomment if saving every second frame
        imwrite(image_data', image_name)
    %writeVideo(writerObj,image_data);
%     end
    
    % print progress out to user
    if mod(i,30)==0
        disp([num2str(floor(100*i/frames)), '% complete'])
    end
end

disp('100% complete!')

close(writerObj);

% close binary file
fclose(fid);

