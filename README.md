MATALB FILE CONVERTER
=====================

Matlab script file for converting EngenderSoft binary image data to an image sequence

## Instructions for use

1. modify the name of binary file to be opened.
	The binary file must be located in the working directory
	unless you specify the full path in the file name.
	
2. Modify the for loop condition so that it runs through the desired number of frames.
	You may want modify the code to just keep reading image data until the entire binary file has been read.
	Be warned however, conversion to a png image sequence is VERY slow.
	
## Binary file format notes

The binary image file created by engendersoft is simply a sequence of grayscale images in the form of 2D 8-bit arrays.
Each 8-bit 2D image array is preceeded by 20 bytes of metadata.
The metadata is intended to be read as 5 32-bit numbers.
The order of the meta data is as follows:

	buffer number => time stamp => last valid buffer => height of image => width of image
	
The size of the 2D 8-bit image arrays can be pre-determined by multiplying the height and width values in the metadata.