function [ number_of_frames ] = get_number_of_frames( fid, file_size, format )
% function to get number of frames stored in binary file

if nargin < 3
    format = 'b'; % big endian (default_
end

meta_data = get_meta_data(fid, format);
frewind(fid); % move back to the start of the file

number_of_frames = file_size/((prod(meta_data(4:5)))+20);

end

