function [ frame_data ] = get_image_data( fid, meta_data, format )
% reads binary image data given a preceeding meta data set

if nargin < 3
    format = 'b'; %big endian format by default
end

frame_data = uint8(fread(fid, flip(meta_data(4:5))', 'uint8=>unit8', 0, format));

end

