function [ meta_data ] = get_meta_data( fid, format )
% read 20 bytes of meta data from binary file
if nargin < 2
format = 'b'; % big endian format by default
end
meta_data = fread(fid, 5, 'uint32', 0, format);
end

